import "bootstrap/dist/css/bootstrap.min.css";
import Body from "./components/body/Body";
import Title from "./components/title/Title";



function App() {
  return (
    <div className="container mt-5">
        <Title />
        <Body />
    </div>
  );
}

export default App;
