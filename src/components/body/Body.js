import React, { Component } from "react";
import InputMessage from "./body-input/InputMessage";
import LikeImage from "./body-output/LikeImage";

class Body extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            ouputMessage: [],
            likeDisplay: false
        }
    }

    // Tạo 1 hàm cho phép thay đổi state inputMessage dựa vào chuỗi truyền vào
    inputMessageChangeHandler = (value) => {
        this.setState({
            inputMessage: value
        })
    }

    // Tạo 1 hàm cho phép thay đổi state ouputMessage và likeDisplay
    outputMessageChangeHandler = () => {
        if(this.state.inputMessage) {
            this.setState({
                ouputMessage: [...this.state.ouputMessage ,this.state.inputMessage],
                likeDisplay: true
            })
        }
    }

    render() {
        return(
            <React.Fragment>
                <InputMessage inputMessageProp={this.state.inputMessage} inputMessageChangeHandlerProp={this.inputMessageChangeHandler} outputMessageChangeHandlerProp={this.outputMessageChangeHandler}/>
                <LikeImage outputMessageProp={this.state.ouputMessage} likeDisplayProp={this.state.likeDisplay} />
            </React.Fragment>
        )
    }
}

export default Body;