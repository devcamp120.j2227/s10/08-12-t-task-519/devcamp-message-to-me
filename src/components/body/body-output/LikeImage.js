import { Component } from "react";

import likeImage from "../../../assets/images/like.png";

class LikeImage extends Component {
    render() {
        // console.log(this.props);
        const {outputMessageProp, likeDisplayProp} = this.props;

        return(
            <div className="mt-3 text-center">
                {outputMessageProp.map((value, index) => {
                    return  <p key={index}>{value}</p>
                })}
               

                {/* Quy định việc ẩn hiện */}
                { likeDisplayProp ? <img src={likeImage} alt="Like" width={100}/> : null }
                
            </div>
        )
    }
}

export default LikeImage;